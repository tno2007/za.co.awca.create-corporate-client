﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using co.za.awca.sqlmappings.Methods;

namespace za.co.awca.create_corporate_client
{

	class Program
	{	
		public static void Main(string[] args)
		{
			int loginId = 2;
			

			
			
			// **************
			
			// Populate thye helper object
			var helper = new UserLogin().
			
			// Populate a client object
			var client = new Client
			{
				InName = "",
				InSurname = "",
				InAge = 21,
				InLoginId = loginId,
				//OutClientId = null,
				//OutSuccess = null,
				//OutSuccessMessage = null
			};
			
			var dsClient = helper.SaveClient(client);
			
			var aClientId = client.OutClientId;
			var aSuccess = client.OutSuccess;
			var aMessage = client.OutSuccessMessage;
			
			// **************
						
			//var command = new SqlCommand();
			//command.CommandText = "SELECT * from GroupData.sys.parameters where object_id = object_id('GroupData.dbo.pGLUserLogin')";
			//command.CommandType = CommandType.Text;
			
			var spModel = helper.GenerateModel("pGLUserLogin");
			
			var sb = new StringBuilder();
			
			sb.AppendLine();
			sb.AppendLine();
			
			foreach (DataRow row in spModel.Tables[0].Rows) {
				
				/*
				sb.AppendFormat("new SqlParameter {{ ParameterName = \"{0}\", SqlDbType = SqlDbType.{1}, Direction = ParameterDirection.{2}, Size = {3}, Value = typeof(DBNull) }},", 
				                row["name"], 
				                ToSqlDbType( Convert.ToInt32(row["system_type_id"])),
				                Convert.ToInt32(row["is_output"]) == 1 ? "Output" : "Input",
				                Convert.ToInt32(row["max_length"])
				               );
				*/
				sb.AppendFormat("new SqlParameter {{ ParameterName = \"{0}\", SqlDbType = SqlDbType.{1}, Direction = ParameterDirection.{2}, Size = {3}, Value = corporate.{4} }},", 
				                row["name"], 
				                ToSqlDbType( Convert.ToInt32(row["system_type_id"])),
				                Convert.ToInt32(row["is_output"]) == 1 ? "Output" : "Input",
				                Convert.ToInt32(row["max_length"]),
				                row["name"].ToString().Replace("@","")
				               );
				
				sb.AppendLine();
			}
			
			Console.WriteLine(sb.ToString());
			
			using(var writetext = new StreamWriter("c:\\temp\\sql.txt"))
			{
				writetext.WriteLine(sb.ToString());
			}
			
			var json = Newtonsoft.Json.JsonConvert.SerializeObject(client);
			
			Console.WriteLine(json);
			Console.ReadKey(true);
		}
		

		

		
		public static SqlDbType ToSqlDbType(int intType)
		{
			var typeMap = new Dictionary<int, SqlDbType>();

			typeMap[34] = SqlDbType.Image;
			typeMap[35] = SqlDbType.Text;
			typeMap[36] = SqlDbType.UniqueIdentifier;
			typeMap[40] = SqlDbType.Date;
			typeMap[41] = SqlDbType.Time;
			typeMap[42] = SqlDbType.DateTime2;
			typeMap[43] = SqlDbType.DateTimeOffset;
			typeMap[48] = SqlDbType.TinyInt;
			typeMap[52] = SqlDbType.SmallInt;
			typeMap[56] = SqlDbType.Int;
			typeMap[58] = SqlDbType.SmallDateTime;
			typeMap[59] = SqlDbType.Real;
			typeMap[60] = SqlDbType.Money;
			typeMap[61] = SqlDbType.DateTime;
			typeMap[62] = SqlDbType.Float;
			typeMap[98] = SqlDbType.Variant;
			typeMap[99] = SqlDbType.NText;
			typeMap[104] = SqlDbType.Bit;
			typeMap[106] = SqlDbType.Decimal;
			typeMap[108] = SqlDbType.Int;
			typeMap[122] = SqlDbType.SmallMoney;
			typeMap[127] = SqlDbType.BigInt;
			typeMap[165] = SqlDbType.VarBinary;
			typeMap[167] = SqlDbType.VarChar;
			typeMap[173] = SqlDbType.Binary;
			typeMap[175] = SqlDbType.Char;
			typeMap[189] = SqlDbType.Timestamp;
			typeMap[231] = SqlDbType.NVarChar;			
			typeMap[239] = SqlDbType.NChar;
			typeMap[241] = SqlDbType.Xml;


			return typeMap[(intType)];
		}
	}
}