﻿using System;

namespace za.co.awca.create_corporate_client
{
	/// <summary>
	/// Description of Client.
	/// </summary>
	public class Client
	{
		public string InName { get; set; }
		public string InSurname { get; set; }
		public int? InAge { get; set; }
		public int? InLoginId { get; set; }
		public int? OutClientId { get; set; }
		public bool? OutSuccess { get; set; }
		public string OutSuccessMessage { get; set; }
		
		public Client()
		{
		}
	}
}
