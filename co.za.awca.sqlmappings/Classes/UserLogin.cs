﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace co.za.awca.sqlmappings.Classes
{
	/// <summary>
	/// Description of UserLogin.
	/// </summary>
	public class UserLogin
	{
		public string VvcLoginEmail { get; set; }
		public string VvcEncLoginPassword { get; set; }
		public string VvcSystem { get; set; }
		public string VvcIPAddress { get; set; }
		public string VvcSessionID { get; set; }
		public string VvcLanguageCode { get; set; }
		
		public UserLogin()
		{
		}
	}
}