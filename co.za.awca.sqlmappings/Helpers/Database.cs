﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace co.za.awca.sqlmappings.Helpers
{
	/// <summary>
	/// Description of Database.
	/// </summary>
	public class Database
	{
		public static string ConnectionString { get; set; }
		
		public Database()
		{
			ConnectionString = "Server=CPT-DEVDB01;Database=GroupData;Trusted_Connection=True;";
		}
		
        public static DataSet Execute(SqlCommand sqlCommand)
        {
        	var dataSet = new DataSet();
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (sqlCommand)
                {
                	sqlCommand.Connection = connection;
                	using (var sqlDataAdapter = new SqlDataAdapter(sqlCommand))
                	{
                		sqlDataAdapter.Fill(dataSet);
                	}
                }
            }
            
            return dataSet;
        }
        
		public static object GetParam(SqlCommand sqlCommand, string param)
		{
			try
			{
				if(sqlCommand.Parameters[param].Value == DBNull.Value)
				{
					return null;
				}
				else
				{
					return sqlCommand.Parameters[param].Value;	
				}
			}
			catch(IndexOutOfRangeException)
			{
				return null;
			}
			catch(Exception ex)
			{
				throw ex;
			}	
		}
		
	    public static DataSet GenerateModel(string databaseName, string storedProcName)
	    {
	        var sqlCommand = new SqlCommand
	        {
	        	CommandText = string.Format("SELECT * from {0}.sys.parameters where object_id = object_id('{1}')", databaseName, storedProcName),
				CommandType = CommandType.Text
	        };
	
	        return Execute(sqlCommand);
	    }
	    
		public static SqlDbType ToSqlDbType(int intType)
		{
			var typeMap = new Dictionary<int, SqlDbType>();

			typeMap[34] = SqlDbType.Image;
			typeMap[35] = SqlDbType.Text;
			typeMap[36] = SqlDbType.UniqueIdentifier;
			typeMap[40] = SqlDbType.Date;
			typeMap[41] = SqlDbType.Time;
			typeMap[42] = SqlDbType.DateTime2;
			typeMap[43] = SqlDbType.DateTimeOffset;
			typeMap[48] = SqlDbType.TinyInt;
			typeMap[52] = SqlDbType.SmallInt;
			typeMap[56] = SqlDbType.Int;
			typeMap[58] = SqlDbType.SmallDateTime;
			typeMap[59] = SqlDbType.Real;
			typeMap[60] = SqlDbType.Money;
			typeMap[61] = SqlDbType.DateTime;
			typeMap[62] = SqlDbType.Float;
			typeMap[98] = SqlDbType.Variant;
			typeMap[99] = SqlDbType.NText;
			typeMap[104] = SqlDbType.Bit;
			typeMap[106] = SqlDbType.Decimal;
			typeMap[108] = SqlDbType.Int;
			typeMap[122] = SqlDbType.SmallMoney;
			typeMap[127] = SqlDbType.BigInt;
			typeMap[165] = SqlDbType.VarBinary;
			typeMap[167] = SqlDbType.VarChar;
			typeMap[173] = SqlDbType.Binary;
			typeMap[175] = SqlDbType.Char;
			typeMap[189] = SqlDbType.Timestamp;
			typeMap[231] = SqlDbType.NVarChar;			
			typeMap[239] = SqlDbType.NChar;
			typeMap[241] = SqlDbType.Xml;

			return typeMap[(intType)];
		}
	}
}
