﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace co.za.awca.sqlmappings.Methods
{
	/// <summary>
	/// Description of UserLogin.
	/// </summary>
	public static class UserLogin
	{
	    public static DataSet Go(Classes.UserLogin userLogin)
	    {
	    	// Create and initialize new sqlCommand object
	        var sqlCommand = new SqlCommand
	        {
	            CommandText = "_LyallTest",
	            CommandType = CommandType.StoredProcedure
	        };
	
	        // Populate the sqlCommand object
	        sqlCommand.Parameters.AddRange(new []
	        {
				new SqlParameter { ParameterName = "@VvcLoginEmail", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 100, Value = userLogin.VvcLoginEmail },
				new SqlParameter { ParameterName = "@VvcEncLoginPassword", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 100, Value = userLogin.VvcEncLoginPassword },
				new SqlParameter { ParameterName = "@VvcSystem", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 20, Value = userLogin.VvcSystem },
				new SqlParameter { ParameterName = "@VvcIPAddress", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 20, Value = userLogin.VvcIPAddress },
				new SqlParameter { ParameterName = "@VvcSessionID", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 50, Value = userLogin.VvcSessionID },
				new SqlParameter { ParameterName = "@VvcLanguageCode", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Size = 2, Value = userLogin.VvcLanguageCode }
	        });
	
			// return results as Dataset
	        var dataSet = Helpers.Database.Execute(sqlCommand);
	        
			// Assign output parameters
			client.OutClientId = (int?)GetParam(sqlCommand, "@OutClientId");
			client.OutSuccess = (bool?)GetParam(sqlCommand, "@OutSuccess");
			client.OutSuccessMessage = (string)GetParam(sqlCommand, "@OutSuccessMessage");
			
			return dataSet;
	    }
	}
}
